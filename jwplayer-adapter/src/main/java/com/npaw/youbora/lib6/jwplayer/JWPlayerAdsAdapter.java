package com.npaw.youbora.lib6.jwplayer;


import com.jwplayer.pub.api.JWPlayer;
import com.jwplayer.pub.api.events.AdBreakEndEvent;
import com.jwplayer.pub.api.events.AdClickEvent;
import com.jwplayer.pub.api.events.AdCompleteEvent;
import com.jwplayer.pub.api.events.AdErrorEvent;
import com.jwplayer.pub.api.events.AdImpressionEvent;
import com.jwplayer.pub.api.events.AdPauseEvent;
import com.jwplayer.pub.api.events.AdPlayEvent;
import com.jwplayer.pub.api.events.AdRequestEvent;
import com.jwplayer.pub.api.events.AdSkippedEvent;
import com.jwplayer.pub.api.events.AdTimeEvent;
import com.jwplayer.pub.api.events.EventType;
import com.jwplayer.pub.api.events.listeners.AdvertisingEvents;
import com.npaw.youbora.lib6.adapter.AdAdapter;

import java.util.Map;

/**
 * Created by Enrique on 25/10/2017.
 */

public class JWPlayerAdsAdapter extends AdAdapter<JWPlayer> implements
        AdvertisingEvents.OnAdSkippedListener, AdvertisingEvents.OnAdCompleteListener,
        AdvertisingEvents.OnAdErrorListener, AdvertisingEvents.OnAdPauseListener,
        AdvertisingEvents.OnAdPlayListener, AdvertisingEvents.OnAdRequestListener,
        AdvertisingEvents.OnAdTimeListener, AdvertisingEvents.OnAdImpressionListener,
        AdvertisingEvents.OnAdClickListener, AdvertisingEvents.OnAdBreakEndListener {

    private AdPosition lastReportedAdPosition;
    private String lastReportedAdResource;
    private Double lastReportedAdPlayhead;

    private boolean firstQuartileFired;
    private boolean secondQuartileFired;
    private boolean thirdQuartileFired;

    public JWPlayerAdsAdapter(JWPlayer jwPlayer) {
        super(jwPlayer);
        resetValues();
        registerListeners();
    }

    @Override
    public void registerListeners() {
        super.registerListeners();

        if (getPlayer() != null) {
            getPlayer().addListeners(this,
                    EventType.AD_SKIPPED,
                    EventType.AD_COMPLETE,
                    EventType.AD_ERROR,
                    EventType.AD_PAUSE,
                    EventType.AD_PLAY,
                    EventType.AD_REQUEST,
                    EventType.AD_TIME,
                    EventType.AD_IMPRESSION,
                    EventType.AD_CLICK,
                    EventType.AD_BREAK_END
            );
        }
    }

    @Override
    public void unregisterListeners() {
        super.unregisterListeners();

        // Unregister listeners
        if (getPlayer() != null) {
            getPlayer().removeListeners(this,
                    EventType.AD_SKIPPED,
                    EventType.AD_COMPLETE,
                    EventType.AD_ERROR,
                    EventType.AD_PAUSE,
                    EventType.AD_PLAY,
                    EventType.AD_REQUEST,
                    EventType.AD_TIME,
                    EventType.AD_IMPRESSION,
                    EventType.AD_CLICK,
                    EventType.AD_BREAK_END
            );
        }
    }

    private void resetValues() {
        lastReportedAdPosition = AdPosition.UNKNOWN;
        lastReportedAdResource = super.getResource();
        lastReportedAdPlayhead = super.getPlayhead();
        firstQuartileFired = false;
        secondQuartileFired = false;
        thirdQuartileFired = false;
    }

    @Override
    public String getResource() { return lastReportedAdResource; }

    @Override
    public AdPosition getPosition() { return lastReportedAdPosition; }

    @Override
    public Double getPlayhead() { return lastReportedAdPlayhead; }

    @Override
    public String getPlayerVersion() {
        String playerVersion = super.getPlayerVersion();

        if (getPlayer() != null) playerVersion = getPlayer().getVersionCode();

        return playerVersion;
    }

    @Override
    public String getVersion() { return BuildConfig.VERSION_NAME + "-" + getPlayerName(); }

    @Override
    public Double getDuration() {
        Double duration = super.getDuration();

        if (getPlayer() != null) duration = getPlayer().getDuration();

        return duration; }

    @Override
    public String getPlayerName() { return "JWPlayer"; }

    @Override
    public void fireStop(Map<String, String> params) {
        super.fireStop(params);
        resetValues();
    }

    @Override
    public void onAdBreakEnd(AdBreakEndEvent adBreakEndEvent) {
        fireAdBreakStop();
    }

    @Override
    public void onAdClick(AdClickEvent adClickEvent) {
        fireClick(adClickEvent.getTag());
    }

    @Override
    public void onAdComplete(AdCompleteEvent adCompleteEvent) {
        fireStop();
        resetValues();
    }

    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        fireError(adErrorEvent.getMessage(), adErrorEvent.getMessage(),
                String.format("{+\"tag\":\"%s\"}", adErrorEvent.getMessage()));
    }

    @Override
    public void onAdImpression(AdImpressionEvent adImpressionEvent) {
        fireStart();
    }

    @Override
    public void onAdPause(AdPauseEvent adPauseEvent) {
        firePause();
    }

    @Override
    public void onAdPlay(AdPlayEvent adPlayEvent) {
        lastReportedAdResource = adPlayEvent.getTag();
        fireResume();
    }

    @Override
    public void onAdRequest(AdRequestEvent adRequestEvent) {
        lastReportedAdPosition = AdPosition.UNKNOWN;

        if (adRequestEvent.getAdPosition() == com.jwplayer.pub.api.media.ads.AdPosition.PRE)
            lastReportedAdPosition = AdPosition.PRE;

        if (adRequestEvent.getAdPosition() == com.jwplayer.pub.api.media.ads.AdPosition.MID)
            lastReportedAdPosition = AdPosition.MID;

        if (adRequestEvent.getAdPosition() == com.jwplayer.pub.api.media.ads.AdPosition.POST)
            lastReportedAdPosition = AdPosition.POST;

        lastReportedAdResource = adRequestEvent.getTag();
    }

    @Override
    public void onAdSkipped(AdSkippedEvent adSkippedEvent) {
        fireSkip();
    }

    @Override
    public void onAdTime(AdTimeEvent adTimeEvent) {
        lastReportedAdPlayhead = adTimeEvent.getPosition();

        if (getPlayhead() != null && getDuration() != null) {
            if (getPlayhead() > getDuration() * 0.25 && !firstQuartileFired) {
                fireQuartile(1);
                firstQuartileFired = true;
            } else if (getPlayhead() > getDuration() * 0.5 && !secondQuartileFired) {
                fireQuartile(2);
                secondQuartileFired = true;
            } else if (getPlayhead() > getDuration() * 0.75 && !thirdQuartileFired) {
                fireQuartile(3);
                thirdQuartileFired = true;
            }
        }

        if (!getFlags().isJoined()) fireJoin();
    }
}
