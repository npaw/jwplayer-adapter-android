package com.npaw.youbora.lib6.jwplayer;


import com.jwplayer.pub.api.JWPlayer;
import com.jwplayer.pub.api.events.AdBreakEndEvent;
import com.jwplayer.pub.api.events.BufferEvent;
import com.jwplayer.pub.api.events.CompleteEvent;
import com.jwplayer.pub.api.events.ErrorEvent;
import com.jwplayer.pub.api.events.EventType;
import com.jwplayer.pub.api.events.IdleEvent;
import com.jwplayer.pub.api.events.PauseEvent;
import com.jwplayer.pub.api.events.PlayEvent;
import com.jwplayer.pub.api.events.PlaylistItemEvent;
import com.jwplayer.pub.api.events.SeekEvent;
import com.jwplayer.pub.api.events.SeekedEvent;
import com.jwplayer.pub.api.events.SetupErrorEvent;
import com.jwplayer.pub.api.events.TimeEvent;
import com.jwplayer.pub.api.events.listeners.AdvertisingEvents;
import com.jwplayer.pub.api.events.listeners.VideoPlayerEvents;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by Enrique on 16/10/2017.
 */

public class JWPlayerAdapter extends PlayerAdapter<JWPlayer> implements
        VideoPlayerEvents.OnCompleteListener, VideoPlayerEvents.OnTimeListener,
        VideoPlayerEvents.OnSeekedListener, VideoPlayerEvents.OnErrorListener,
        VideoPlayerEvents.OnIdleListener, VideoPlayerEvents.OnBufferListener,
        VideoPlayerEvents.OnPauseListener, VideoPlayerEvents.OnPlayListener,
        VideoPlayerEvents.OnPlaylistItemListener, VideoPlayerEvents.OnSetupErrorListener,
        VideoPlayerEvents.OnSeekListener, AdvertisingEvents.OnAdBreakEndListener {

    private Double lastReportedPlayhead;
    private String lastReportedTitle;
    private String lastReportedResource;
    private Boolean skipNextBuffer;
    private Integer currentPlaylistIndex;

    public JWPlayerAdapter(JWPlayer jwPlayer) {
        super(jwPlayer);
        currentPlaylistIndex = 0;
        skipNextBuffer = false;
        registerListeners();
    }

    @Override
    public void registerListeners() {
        super.registerListeners();

        // Add observers
        if (getPlayer() != null) {
            getPlayer().addListeners(this,
                    EventType.COMPLETE,
                    EventType.TIME,
                    EventType.SEEKED,
                    EventType.ERROR,
                    EventType.IDLE,
                    EventType.BUFFER,
                    EventType.PAUSE,
                    EventType.PLAY,
                    EventType.PLAYLIST_ITEM,
                    EventType.SETUP_ERROR,
                    EventType.SEEK,
                    EventType.AD_BREAK_END
                    );
        }
    }

    @Override
    public void unregisterListeners() {
        super.unregisterListeners();

        // Remove observers
        if (getPlayer() != null) {
            getPlayer().removeListeners(this,
                    EventType.COMPLETE,
                    EventType.TIME,
                    EventType.SEEKED,
                    EventType.ERROR,
                    EventType.IDLE,
                    EventType.BUFFER,
                    EventType.PAUSE,
                    EventType.PLAY,
                    EventType.PLAYLIST_ITEM,
                    EventType.SETUP_ERROR,
                    EventType.SEEK,
                    EventType.AD_BREAK_END
            );
        }
    }

    @Override
    public void onSeek(SeekEvent seekEvent) { fireSeekBegin(); }

    @Override
    public void onSeeked(SeekedEvent seekedEvent) {
        //fireSeekEnd();
    }

    @Override
    public void onError(ErrorEvent errorEvent) {
        fireFatalError(errorEvent.getMessage(), errorEvent.getMessage(), null);
    }

    @Override
    public void onIdle(IdleEvent idleEvent) {
        YouboraLog.debug("JWP-onIdle");
        // fireStop();
    }

    @Override
    public void onBuffer(BufferEvent bufferEvent) {
        if(!skipNextBuffer){
            fireBufferBegin();
        }
        skipNextBuffer = false;
    }

    @Override
    public void onPause(PauseEvent pauseEvent) {
        firePause();
    }

    @Override
    public void onPlay(PlayEvent playEvent) {
        YouboraLog.debug("JWP-OnPlayEvent");
        fireBufferEnd();
        fireSeekEnd();
        fireResume();
        fireStart();
    }

    @Override
    public void onPlaylistItem(PlaylistItemEvent playlistItemEvent) {
        YouboraLog.debug("JWP-onPlaylistItem");
        lastReportedResource = playlistItemEvent.getPlaylistItem().getFile();
        lastReportedTitle = playlistItemEvent.getPlaylistItem().getTitle();

        if(currentPlaylistIndex != playlistItemEvent.getIndex()){
            fireStop();
        }
    }

    @Override
    public void onSetupError(SetupErrorEvent setupErrorEvent) {
        fireFatalError(setupErrorEvent.getMessage(), setupErrorEvent.getMessage(),null);
    }

    @Override
    public Double getPlayhead() { return lastReportedPlayhead; }
    @Override
    public String getTitle() { return lastReportedTitle; }

    @Override
    public String getResource() { return lastReportedResource; }

    @Override
    public Double getDuration() {
        Double duration = super.getDuration();

        if (getPlayer() != null) duration = getPlayer().getDuration();

        return duration;
    }

    @Override
    public Boolean getIsLive() {
        Boolean isLive = super.getIsLive();

        if (getDuration() != null) isLive = getDuration() == -1.0;

        return isLive;
    }

    @Override
    public String getPlayerVersion() {
        String playerVersion = super.getPlayerVersion();

        if (getPlayer() != null) playerVersion = getPlayer().getVersionCode();

        return playerVersion;
    }

    @Override
    public String getVersion() { return BuildConfig.VERSION_NAME + "-" + getPlayerName(); }

    private void resetValues() {
        lastReportedPlayhead = super.getPlayhead();
        lastReportedTitle = super.getTitle();
    }

    @Override
    public String getPlayerName() { return "JWPlayer"; }

    @Override
    public void fireStop(Map<String, String> params) {
        super.fireStop(params);
        resetValues();
    }

    @Override
    public void onComplete(CompleteEvent completeEvent) {
        YouboraLog.debug("JWP-onComplete");
        fireStop();
    }

    @Override
    public void onTime(TimeEvent timeEvent) {
        lastReportedPlayhead = timeEvent.getPosition();

        if (!getFlags().isJoined()) fireJoin();
    }

    @Override
    public void fireStart(@NotNull Map<String, String> params) {
        if (getPlayer() != null)
            currentPlaylistIndex = getPlayer().getPlaylistIndex();

        super.fireStart(params);
    }

    @Override
    public void onAdBreakEnd(AdBreakEndEvent adBreakEndEvent) {
        if(getFlags().isJoined()) skipNextBuffer = true;
    }
}
