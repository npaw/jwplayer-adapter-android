package com.npaw.youbora.lib6.jwplayer.demo;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.jwplayer.pub.api.JWPlayer;
import com.jwplayer.pub.api.configuration.PlayerConfig;
import com.jwplayer.pub.api.configuration.ads.VastAdvertisingConfig;
import com.jwplayer.pub.api.events.FullscreenEvent;
import com.jwplayer.pub.api.events.listeners.VideoPlayerEvents;
import com.jwplayer.pub.api.license.LicenseUtil;
import com.jwplayer.pub.api.media.ads.AdBreak;
import com.jwplayer.pub.api.media.playlists.PlaylistItem;
import com.jwplayer.pub.view.JWPlayerView;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.jwplayer.JWPlayerAdapter;
import com.npaw.youbora.lib6.jwplayer.JWPlayerAdsAdapter;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.utils.youboraconfigutils.YouboraConfigManager;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements VideoPlayerEvents.OnFullscreenListener {
	private JWPlayerView playerView;
	private CoordinatorLayout coordinatorLayout;
	private JWPlayer mPlayer;

	private Plugin plugin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		new LicenseUtil().setLicenseKey(this, "ztXfLclg4SNiU+Eq09KXZK8r5wQSDjZY5vCjwg==");

		playerView = findViewById(R.id.playerView);
		coordinatorLayout = findViewById(R.id.activity_jwplayerview);
		mPlayer = playerView.getPlayer();

		// Set the ad break offset, note the ad tag variables in our ad tag URL
		List<AdBreak> schedule = new LinkedList<>();
		AdBreak.Builder adBreakBuilder = new AdBreak.Builder();
		AdBreak adBreak1 = adBreakBuilder.offset("pre").tag("https://playertest.longtailvideo.com/vast-30s-ad.xml").build();
		AdBreak adBreak2 = adBreakBuilder.offset("10%").tag("https://playertest.longtailvideo.com/vast-30s-ad.xml").build();
		AdBreak adBreak4 = adBreakBuilder.offset("post").tag("https://playertest.longtailvideo.com/vast-30s-ad.xml").build();

		schedule.add(adBreak1);
		schedule.add(adBreak2);
		schedule.add(adBreak4);

		// Create your advertising config
		VastAdvertisingConfig advertisingConfig = new VastAdvertisingConfig.Builder()
				.schedule(schedule)
				.build();

		List<PlaylistItem> playlist = new LinkedList<>();

		PlaylistItem pi2 = new PlaylistItem.Builder()
				.file("http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8")
				.title("Apple conference")
				.description("The Apple conference")
				.build();

		PlaylistItem pi1 = new PlaylistItem.Builder()
				.file("http://www.bok.net/dash/tears_of_steel/cleartext/stream.mpd")
				.title("DASH")
				.description(".mpd")
				.build();

		PlaylistItem pi3 = new PlaylistItem.Builder()
				.file("https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8")
				.title("HLS Live")
				.description(".m3u8")
				.build();

		playlist.add(pi1);
		playlist.add(pi2);
		playlist.add(pi3);

		PlayerConfig playerConfig = new PlayerConfig.Builder()
				.playlist(playlist)
				.advertisingConfig(advertisingConfig)
				.build();

		mPlayer.setup(playerConfig);

		// Testeable ui
		final Button buttonPlay = findViewById(R.id.play);
		final Button buttonPause = findViewById(R.id.pause);
		final Button buttonSeek = findViewById(R.id.seekButton);
		final Button buttonRefreshTimes = findViewById(R.id.refreshTimes);
		final EditText seekTime = findViewById(R.id.seekTime);
		final EditText totalTime = findViewById(R.id.totalTime);
		final EditText actualTime = findViewById(R.id.currentTime);

		buttonPlay.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mPlayer.play();
			}
		});

		buttonPause.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mPlayer.pause();
			}
		});

		buttonSeek.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mPlayer.seek(Double.parseDouble(seekTime.getText().toString()));
			}
		});

		buttonRefreshTimes.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				totalTime.setText(Double.toString(mPlayer.getDuration()));
				actualTime.setText(Double.toString(mPlayer.getPosition()));
			}
		});

		initializeNpawPlugin();
	}

	private void initializeNpawPlugin() {
		YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE);
		Options options = YouboraConfigManager.Companion.getInstance().getOptions(this);
		assert options != null;
		options.setAccountCode("");
		plugin = new Plugin(options, getApplicationContext());
		plugin.setActivity(this);
		plugin.setAdapter(new JWPlayerAdapter(mPlayer));
		plugin.setAdsAdapter(new JWPlayerAdsAdapter(mPlayer));
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// Set fullscreen when the device is rotated to landscape
		mPlayer.setFullscreen(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE, true);
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Exit fullscreen when the user pressed the Back button
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (mPlayer.getFullscreen()) {
				mPlayer.setFullscreen(false, false);
				return false;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.youbora_options) {
			YouboraConfigManager.Companion.getInstance().showConfig(this);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onFullscreen(FullscreenEvent fullscreenEvent) {
		boolean fullscreen = fullscreenEvent.getFullscreen();
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null) {
			if (fullscreen)
				actionBar.hide();
			else
				actionBar.show();
		}

		// When going to Fullscreen we want to set fitsSystemWindows="false"
		coordinatorLayout.setFitsSystemWindows(!fullscreen);
	}
}
