## [6.7.5] - 2022-04-08
### Updated
- Update lib version to 6.7.64.

## [6.7.4] - 2022-04-05
### Updated
- Update JWPlayer SDK to 4.5.0.
- Update demo.

## [6.7.3] - 2022-04-05
### Updated
- Update lib version to 6.7.63.
### Modified
- Buffers tracked by player events adding additional conditions instead of monitor.
### Added
- Fire stop when changing element on playlist.
### Fixed
- Fix pipelines.
- Fire error before stop.
- Ignore buffer when there is an Ad transition.

## [6.7.2] - 2022-03-09
### Changed
- Update demo and adapter for JWP v4.
- Set target sdk to (>= 21 && <= 31).
- Update Lib version to 6.7.52.
- Migrate demo to AndroidX.

## [6.7.1] - 2021-04-21
### Modified
- Buffers tracked by monitor instead of player events.
- Deployment platform moved from Bintray to JFrog.

## [6.7.0] - 2020-03-16
### Refactored
- Adapter to work with the new Youboralib version.

## [6.5.0] - 2019-08-05
### Added
- Ads rework.

## [6.3.1] - 2019-02-13
### Fixed
- Seek should be now reported correctly
- If the video is whether live or VOD is now reported correctly
- adStart sent when back from the background

## [6.3.0] - 2018-12-18
### Added
- Upgraded to JWPlayer 3.3.0

## [6.0.2] - 2018-08-09
### Added
- Upgraded to JWPlayer v3.1.0

## [6.0.1] - 2018-01-03
### Added
 - Version name to gradle
### Fixed
 - Version not being displayed correctly
 
## [6.0.0] - 2017-11-10
 - Release